#ifndef UTIL_COMPARISION_CHECK_H
#define UTIL_COMPARISION_CHECK_H

#include <gtest/gtest.h>

template <class T>
void CheckComparisionEqual(const T& lhs, const T& rhs) {
    ASSERT_TRUE(lhs == rhs);
    ASSERT_TRUE(lhs <= rhs);
    ASSERT_TRUE(lhs >= rhs);
    ASSERT_FALSE(lhs != rhs);
    ASSERT_FALSE(lhs < rhs);
    ASSERT_FALSE(lhs > rhs);
}

template <class T>
void CheckComparisionLess(const T& lhs, const T& rhs) {
    ASSERT_FALSE(lhs == rhs);
    ASSERT_TRUE(lhs <= rhs);
    ASSERT_FALSE(lhs >= rhs);
    ASSERT_TRUE(lhs != rhs);
    ASSERT_TRUE(lhs < rhs);
    ASSERT_FALSE(lhs > rhs);
}

template <class T>
void CheckComparisionGreater(const T& lhs, const T& rhs) {
    ASSERT_FALSE(lhs == rhs);
    ASSERT_FALSE(lhs <= rhs);
    ASSERT_TRUE(lhs >= rhs);
    ASSERT_TRUE(lhs != rhs);
    ASSERT_FALSE(lhs < rhs);
    ASSERT_TRUE(lhs > rhs);
}

#endif  // UTIL_COMPARISION_CHECK_H