#ifndef UTIL_APPEND_H
#define UTIL_APPEND_H

template <class Container>
void AppendBack(Container&) {
}

template <class Container>
void AppendFront(Container&) {
}

template <class Container, class T, class... Args>
void AppendBack(Container& v, const T& value, const Args&... args) {
    v.PushBack(value);
    AppendBack(v, args...);
}

template <class Container, class T, class... Args>
void AppendFront(Container& v, const T& value, const Args&... args) {
    v.PushFront(value);
    AppendFront(v, args...);
}

#endif  // UTIL_APPEND_H
