#ifndef UTIL_CONSTANTS_H
#define UTIL_CONSTANTS_H

template <class T>
inline const T kZero = T(0);

template <class T>
inline const T kOne = T(1);

#endif  // UTIL_CONSTANTS_H