#ifndef UTIL_RANGE_EQUAL_CHECK_H
#define UTIL_RANGE_EQUAL_CHECK_H

#include <gtest/gtest.h>
#include <array>

template <class Container, class T, size_t N>
void EqualRange(const Container& container, const std::array<T, N>& required) {
    ASSERT_TRUE(container.Size() == N);
    for (size_t i = 0u; i < N; ++i) {
        ASSERT_TRUE(container[i] == required[i]);
    }
}

#endif  // UTIL_RANGE_EQUAL_CHECK_H