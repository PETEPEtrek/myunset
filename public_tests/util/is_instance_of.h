#ifndef UTIL_IS_INSTANCE_OF_H
#define UTIL_IS_INSTANCE_OF_H

template <class T, template <class...> class Template>
struct IsInstanceOf {
    static constexpr bool kValue = false;
};

template <class... Types, template <class...> class Template>
struct IsInstanceOf<Template<Types...>, Template> {
    static constexpr bool kValue = true;
};

template <class T, template <class...> class Template>
inline constexpr bool kIsInstanceOfV = IsInstanceOf<T, Template>::kValue;

#endif  // UTIL_IS_INSTANCE_OF_H
