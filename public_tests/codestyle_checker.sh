#!/bin/bash

if [[ "$#" -lt 1 ]]
then
	echo "error: no input files"
	exit 1
fi

SOLUTION_DIRECTORY=$1

python run-clang-format.py -r ../$SOLUTION_DIRECTORY
if [[ $? -ne 0 ]]
then
        echo "Problems with codestyle"
        exit 1
fi

if [ `find ../$SOLUTION_DIRECTORY -maxdepth 1 -name "*.cpp" | wc -l` -ne 0 ]; then
	clang-tidy --extra-arg=-std=c++17 -p ./$SOLUTION_DIRECTORY/build ../$SOLUTION_DIRECTORY/*.cpp
fi

if [[ $? -ne 0 ]]
then
        echo "Problems with codestyle"
        exit 1
fi

if [ `find ../$SOLUTION_DIRECTORY -maxdepth 1 -name "*.h" | wc -l` -ne 0 ]; then
        clang-tidy --extra-arg=-std=c++17 -p ./$SOLUTION_DIRECTORY/build ../$SOLUTION_DIRECTORY/*.h
fi

if [[ $? -ne 0 ]]
then
        echo "Problems with codestyle"
        exit 1
fi

#kek
