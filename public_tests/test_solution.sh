#!/bin/bash

if [[ "$#" -lt 1 ]]
then 
	echo "What problem?"
	exit 1
fi

PROBLEM_NAME=$1

if [[ $PROBLEM_NAME == "master" ]]
then
	exit 0
fi

if [[ `find . -maxdepth 1 -type d -name "$PROBLEM_NAME" | wc -l` -eq 0 ]]
then
	echo "Didn't find this problem."
	exit 1
fi

cd $PROBLEM_NAME

if [[ `find . -maxdepth 1 -type d -name "build" | wc -l` -eq 0 ]]
then
mkdir build
fi

cd build
cmake ..
if [[ $? -ne 0 ]]
then
	echo "File organization is incorrect."
	exit 1
fi
make
if [[ $? -ne 0 ]]
then
	echo "Doesn't even compile..."
	exit 1
fi
echo "Checking codestyle..."
cd ../..
./codestyle_checker.sh $PROBLEM_NAME
if [[ $? -ne 0 ]]
then
	echo "Problems with codestyle."
	exit 1
fi
echo "Running tests..."
timeout -s SIGKILL 2m valgrind --leak-check=yes --log-file=log.txt ./$PROBLEM_NAME/build/"$PROBLEM_NAME"_public_test
if [[ $? -ne 0 ]]
then
        echo "Ooops, there are failed tests :("
        exit 1
fi

echo "Valgrind log:"
cat log.txt
python valgrind_parser.py
if [[ $? -ne 0 ]]
then
	echo "VALGRIND!!! VALGRIND!!! VALGRIND!!!"
	exit 1
fi
rm log.txt
rm -r $PROBLEM_NAME/build
#kek
