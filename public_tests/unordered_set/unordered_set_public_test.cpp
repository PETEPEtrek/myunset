#include <gtest/gtest.h>

#include <string>
#include <forward_list>
#include <vector>
#include <sstream>
#include <algorithm>

#include "unordered_set.h"

TEST(Constructors, DefaultConstructor) {
    UnorderedSet<int> us;
    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 0u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_FALSE(us.Find(i));
    }
    for (size_t i = 0u; i < 10u; ++i) {
        ASSERT_EQ(us.BucketSize(i), 0u);
    }
    ASSERT_FLOAT_EQ(us.LoadFactor(), 0.0f);
}

TEST(Constructors, BucketCountConstructor) {
    UnorderedSet<std::string> us(5u);
    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 5u);
    for (int i = 0; i < 10; ++i) {
        const auto str = std::to_string(i);
        const auto bucket = std::hash<std::string>{}(str) % 5u;
        ASSERT_EQ(us.Bucket(str), bucket);
        ASSERT_FALSE(us.Find(str));
    }
    for (size_t i = 0u; i < 10u; ++i) {
        ASSERT_EQ(us.BucketSize(i), 0u);
    }
    ASSERT_FLOAT_EQ(us.LoadFactor(), 0.0f);
}

TEST(Constructors, RangeConstructor) {
    {
        std::forward_list<int> fl{0, 1, 2, 3};
        UnorderedSet<int> us(fl.begin(), fl.end());
        ASSERT_EQ(us.Size(), 4u);
        ASSERT_FALSE(us.Empty());
        ASSERT_EQ(us.BucketCount(), 4u);
        for (int i = 0; i < 10; ++i) {
            const auto bucket = std::hash<int>{}(i) % 4u;
            ASSERT_EQ(us.Bucket(i), bucket);
            ASSERT_EQ(us.Find(i), i < 4);
        }
        size_t bucket_size_sum = 0;
        for (size_t i = 0u; i < 10u; ++i) {
            bucket_size_sum += us.BucketSize(i);
        }
        ASSERT_EQ(bucket_size_sum, 4u);
        ASSERT_FLOAT_EQ(us.LoadFactor(), 1.0f);
    }

    {
        std::forward_list<int> fl{0, 1, 2, 3};
        UnorderedSet<int> us(fl.begin(), fl.end(), 8);
        ASSERT_EQ(us.Size(), 4u);
        ASSERT_FALSE(us.Empty());
        ASSERT_EQ(us.BucketCount(), 8u);
        for (int i = 0; i < 10; ++i) {
            const auto bucket = std::hash<int>{}(i) % 8u;
            ASSERT_EQ(us.Bucket(i), bucket);
            ASSERT_EQ(us.Find(i), i < 4);
        }
        size_t bucket_size_sum = 0;
        for (size_t i = 0u; i < 10u; ++i) {
            bucket_size_sum += us.BucketSize(i);
        }
        ASSERT_EQ(bucket_size_sum, 4u);
        ASSERT_FLOAT_EQ(us.LoadFactor(), 0.5f);
    }
}

TEST(Constructors, Copy) {
    std::forward_list<int> fl{0, 1, 2, 3};
    UnorderedSet<int> us(fl.begin(), fl.end());
    const auto us_copy = us;

    ASSERT_TRUE(us.Size() == 4u && us.Size() == us_copy.Size());
    ASSERT_FALSE(us.Empty() || us_copy.Empty());
    ASSERT_TRUE(us.BucketCount() == 4u && us.BucketCount() == us_copy.BucketCount());
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us.Find(i), i < 4);
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }

    us.Clear();
    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    for (int i = 0; i < 10; ++i) {
        ASSERT_FALSE(us.Find(i));
    }

    ASSERT_EQ(us_copy.Size(), 4u);
    ASSERT_FALSE(us_copy.Empty());
    ASSERT_EQ(us_copy.BucketCount(), 4u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }
}

TEST(Constructors, Move) {
    std::forward_list<int> fl{0, 1, 2, 3};
    UnorderedSet<int> us(fl.begin(), fl.end());
    const auto us_copy = std::move(us);

    ASSERT_EQ(us.Size(), 0);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 0u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_FALSE(us.Find(i));
    }

    ASSERT_EQ(us_copy.Size(), 4u);
    ASSERT_FALSE(us_copy.Empty());
    ASSERT_EQ(us_copy.BucketCount(), 4u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }
}
// Size empty clear find bucket bucket_count bucket_size load_factor rehash reserve

TEST(Assignment, Copy) {
    std::forward_list<int> fl{0, 1, 2, 3};
    UnorderedSet<int> us(fl.begin(), fl.end());
    UnorderedSet<int> us_copy(fl.begin(), fl.end());
    us_copy = us;

    ASSERT_TRUE(us.Size() == 4u && us.Size() == us_copy.Size());
    ASSERT_FALSE(us.Empty() || us_copy.Empty());
    ASSERT_TRUE(us.BucketCount() == 4u && us.BucketCount() == us_copy.BucketCount());
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us.Find(i), i < 4);
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }

    us.Clear();
    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    for (int i = 0; i < 10; ++i) {
        ASSERT_FALSE(us.Find(i));
    }

    ASSERT_EQ(us_copy.Size(), 4u);
    ASSERT_FALSE(us_copy.Empty());
    ASSERT_EQ(us_copy.BucketCount(), 4u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }
}

TEST(Assignment, Move) {
    std::forward_list<int> fl{0, 1, 2, 3};
    UnorderedSet<int> us(fl.begin(), fl.end());
    UnorderedSet<int> us_copy(fl.begin(), fl.end());
    us_copy = std::move(us);

    ASSERT_EQ(us.Size(), 0);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 0u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_FALSE(us.Find(i));
    }

    ASSERT_EQ(us_copy.Size(), 4u);
    ASSERT_FALSE(us_copy.Empty());
    ASSERT_EQ(us_copy.BucketCount(), 4u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_EQ(us_copy.Find(i), i < 4);
    }
}

TEST(Usage, Insert) {
    UnorderedSet<std::string> us;
    size_t expected_capacity = 1;
    for (size_t i = 0u; i < 10u; ++i) {
        const auto str = std::to_string(i);
        if (i % 2u == 0u) {
            us.Insert(str);
        } else {
            us.Insert(std::move(str));
        }
        if (i + 1 > expected_capacity) {
            expected_capacity *= 2;
        }
        ASSERT_EQ(us.Size(), i + 1);
        ASSERT_FALSE(us.Empty());
        ASSERT_EQ(us.BucketCount(), expected_capacity);
        ASSERT_FLOAT_EQ(us.LoadFactor(), static_cast<float>(i + 1) / expected_capacity);
    }
    for (size_t i = 0; i < 10u; ++i) {
        const auto str = std::to_string(i);
        ASSERT_TRUE(us.Find(str));
    }
    for (size_t i = 10u; i < 20u; ++i) {
        const auto str = std::to_string(i);
        ASSERT_FALSE(us.Find(str));
    }
}

TEST(Usage, Erase) {
    std::forward_list<size_t> fl{0, 2, 4, 6, 8, 1, 3, 5, 7, 9};
    UnorderedSet<size_t> us(fl.begin(), fl.end());
    for (size_t i = 0; i < 5; ++i) {
        us.Erase(2 * i);
        const auto size = 10u - i - 1u;
        ASSERT_EQ(us.Size(), size);
        ASSERT_FALSE(us.Empty());
    }
    for (size_t i = 0; i < 5; ++i) {
        ASSERT_TRUE(us.Find(2 * i + 1));
        ASSERT_FALSE(us.Find(2 * i));
    }
}

TEST(Usage, CustomHashAndEqual) {
    struct DummyHash {
        size_t operator()(int) const noexcept {
            return 0;
        }
    };

    {
        UnorderedSet<int, DummyHash> us;
        for (int i = 0; i < 10; ++i) {
            us.Insert(i);
        }
        ASSERT_EQ(us.Size(), 10u);
        ASSERT_FALSE(us.Empty());
        ASSERT_EQ(us.BucketCount(), 16u);
        for (int i = 0; i < 10; ++i) {
            ASSERT_EQ(us.Bucket(i), 0u);
        }
        for (size_t i = 0u; i < 16u; ++i) {
            ASSERT_EQ(us.BucketSize(i), (i == 0 ? 10u : 0u));
        }
    }

    struct StringSizeHash {
        size_t operator()(const std::string& str) const noexcept {
            return str.size();
        }
    };

    struct StringSizeEqual {
        bool operator()(const std::string& lhs, const std::string& rhs) const noexcept {
            return lhs.size() == rhs.size();
        }
    };

    {
        std::vector<std::string> values{"b", "a", "ab", "ba", "bab", "aba"};
        UnorderedSet<std::string, StringSizeHash, StringSizeEqual> us;
        for (const auto& value : values) {
            us.Insert(value);
        }
        ASSERT_EQ(us.Size(), 3u);
        ASSERT_FALSE(us.Empty());
        ASSERT_EQ(us.BucketCount(), 4u);
    }
}

TEST(Bucket, Rehash) {
    UnorderedSet<int> us;
    us.Rehash(11u);

    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        const auto bucket = std::hash<int>{}(i) % 11u;
        ASSERT_EQ(us.Bucket(i), bucket);
        ASSERT_FALSE(us.Find(i));
    }

    for (int i = 0; i < 10; ++i) {
        us.Insert(i);
    }

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Rehash(5u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Rehash(10u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 10u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Rehash(50u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 50u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }
}

TEST(Bucket, Reserve) {
    UnorderedSet<int> us;
    us.Reserve(11u);

    ASSERT_EQ(us.Size(), 0u);
    ASSERT_TRUE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        const auto bucket = std::hash<int>{}(i) % 11u;
        ASSERT_EQ(us.Bucket(i), bucket);
        ASSERT_FALSE(us.Find(i));
    }

    for (int i = 0; i < 10; ++i) {
        us.Insert(i);
    }

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Reserve(5u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Reserve(10u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 11u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }

    us.Reserve(50u);

    ASSERT_EQ(us.Size(), 10u);
    ASSERT_FALSE(us.Empty());
    ASSERT_EQ(us.BucketCount(), 50u);
    for (int i = 0; i < 10; ++i) {
        ASSERT_TRUE(us.Find(i));
    }
}


int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
