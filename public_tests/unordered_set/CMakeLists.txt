cmake_minimum_required(VERSION 3.0)
project(unordered_set)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror -Wno-self-assign-overloaded -Wno-missing-braces")

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})
enable_testing()

add_executable(unordered_set_public_test unordered_set_public_test.cpp)

target_link_libraries(
        unordered_set_public_test
        Threads::Threads
        ${GTEST_LIBRARIES}
        ${GMOCK_BOTH_LIBRARIES}
        )

include_directories(${CMAKE_SOURCE_DIR}/../../unordered_set ${CMAKE_SOURCE_DIR}/..)

link_libraries(gtest gtest_main pthread)
