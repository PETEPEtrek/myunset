#ifndef UNORDERED_SET_UNORDERED_SET_H
#define UNORDERED_SET_UNORDERED_SET_H

#include <forward_list>
#include <iterator>
#include <list>
#include <vector>

#define YANDEX_CONTEST_REMINDER

template <
    class KeyT,
    class HashT = std::hash<KeyT>,
    class EqualT = std::equal_to<KeyT>>
class UnorderedSet
{
  private:
    std::vector<std::list<KeyT>> buckets_;

  public:
    UnorderedSet()
    {
        buckets_.resize(0);
    }
    UnorderedSet(int32_t count)
    { // NOLINT
        buckets_.resize(count);
    }
    UnorderedSet(
        typename std::forward_list<KeyT>::iterator begin,
        typename std::forward_list<KeyT>::iterator end)
    {
        auto i = begin;
        auto j = end;
        for (; i != j; ++i) {
            Insert(*i);
        }
    }
    UnorderedSet(
        typename std::forward_list<KeyT>::iterator begin,
        typename std::forward_list<KeyT>::iterator end,
        int32_t count)
    {
        auto i = begin;
        auto tmp = begin;
        auto j = end;
        int32_t k = 0;
        for (; tmp != j; ++tmp) {
            ++k;
        }
        Rehash(std::max(k, count));
        for (; i != j; ++i) {
            Insert(*i);
        }
    }
    UnorderedSet(const UnorderedSet& other) = default;

    UnorderedSet(UnorderedSet&& other) = default;

    UnorderedSet& operator=(const UnorderedSet& other) = default;
    UnorderedSet& operator=(UnorderedSet&& other) = default;
    int32_t Size()
    {
        int32_t k = 0;
        for (size_t i = 0; i < buckets_.size(); ++i) {
            k += buckets_[i].size();
        }
        return k;
    }
    int32_t Size() const
    {
        int32_t k = 0;
        for (size_t i = 0; i < buckets_.size(); ++i) {
            k += buckets_[i].size();
        }
        return k;
    }
    int32_t BucketCount()
    {
        return buckets_.size();
    }
    int32_t BucketCount() const
    {
        return buckets_.size();
    }
    int32_t BucketSize(int32_t i)
    {
        if (Empty() || static_cast<uint64_t>(i) >= buckets_.size()) {
            return 0;
        }
        return buckets_[i].size();
    }
    int32_t BucketSize(int32_t i) const
    {
        if (Empty() || static_cast<uint64_t>(i) >= buckets_.size()) {
            return 0;
        }
        return buckets_[i].size();
    }
    int32_t Bucket(KeyT key)
    {
        return HashT()(key) % BucketCount();
    }
    int32_t Bucket(KeyT key) const
    {
        return HashT()(key) % BucketCount();
    }
    double LoadFactor()
    {
        if (Empty()) {
            return 0;
        }
        return Size() / (BucketCount() * 1.0);
    }
    double LoadFactor() const
    {
        if (Empty()) {
            return 0;
        }
        return Size() / (BucketCount() * 1.0);
    }
    bool Empty()
    {
        for (size_t i = 0; i < buckets_.size(); ++i) {
            if (!buckets_[i].empty()) {
                return false;
            }
        }
        return true;
    }
    bool Empty() const
    {
        for (size_t i = 0; i < buckets_.size(); ++i) {
            if (!buckets_[i].empty()) {
                return false;
            }
        }
        return true;
    }
    void Clear()
    {
        for (int32_t i = buckets_.size() - 1; i >= 0; --i) {
            buckets_[i].clear();
        }
        buckets_.clear();
    }
    void Insert(const KeyT& elem)
    {
        if (Find(elem)) {
            return;
        }
        if (buckets_.empty()) {
            buckets_.resize(1);
        }
        if (LoadFactor() >= 1.0) {
            Rehash(buckets_.size() * 2);
        }
        buckets_[HashT()(elem) % buckets_.size()].insert(
            buckets_[HashT()(elem) % buckets_.size()].end(), elem);
    }
    void Insert(KeyT&& elem)
    {
        if (Find(elem)) {
            return;
        }
        if (buckets_.empty()) {
            buckets_.resize(1);
        }
        if (LoadFactor() >= 1.0) {
            Rehash(buckets_.size() * 2);
        }
        buckets_[HashT()(elem) % buckets_.size()].insert(elem);
    }
    void Insert(
        typename std::forward_list<KeyT>::iterator begin,
        typename std::forward_list<KeyT>::iterator end)
    {
        if (buckets_.empty()) {
            buckets_.resize(1);
        }
        double load_factor =
            (Size() + (sizeof(end - begin) / sizeof(begin))) / buckets_.size();
        while ((load_factor >= 1.0)) {
            buckets_.resize(buckets_.size() * 2);
            load_factor = (Size() + (sizeof(end - begin) / sizeof(begin))) /
                          buckets_.size();
        }
        auto i = begin;
        auto j = end;
        for (; i != j; ++i) {
            Insert(*i);
        }
    }
    void Erase(const KeyT& elem)
    {
        for (typename std::list<KeyT>::const_iterator it =
                 buckets_[HashT()(elem) % buckets_.size()].begin();
             it != buckets_[HashT()(elem) % buckets_.size()].end();
             ++it) {
            if (EqualT()(elem, *it)) {
                buckets_[HashT()(elem) % buckets_.size()].erase(it);
                break;
            }
        }
    }
    bool Find(const KeyT& elem)
    {

        if (Empty()) {
            return false;
        }
        for (typename std::list<KeyT>::const_iterator it =
                 buckets_[HashT()(elem) % buckets_.size()].begin();
             it != buckets_[HashT()(elem) % buckets_.size()].end();
             ++it) {
            if (EqualT()(elem, *it)) {
                return true;
            }
        }
        return false;
    }
    bool Find(const KeyT& elem) const
    {
        if (Empty()) {
            return false;
        }
        for (typename std::list<KeyT>::const_iterator it =
                 buckets_[HashT()(elem) % buckets_.size()].begin();
             it != buckets_[HashT()(elem) % buckets_.size()].end();
             ++it) {
            if (EqualT()(elem, *it)) {
                return true;
            }
        }
        return false;
    }
    void Rehash(int32_t new_bucket_count)
    {
        if (static_cast<uint64_t>(new_bucket_count) == buckets_.size() ||
            new_bucket_count < Size()) {
            return;
        }
        UnorderedSet tmp(new_bucket_count);
        for (size_t i = 0; i < buckets_.size(); ++i) {
            for (typename std::list<KeyT>::const_iterator it =
                     buckets_[i].begin();
                 it != buckets_[i].end();
                 ++it) {
                tmp.Insert(*it);
            }
        }
        *this = tmp;
    }
    void Reserve(int32_t new_bucket_count)
    {
        if (static_cast<uint64_t>(new_bucket_count) > buckets_.size()) {
            UnorderedSet tmp(new_bucket_count);
            for (size_t i = 0; i < buckets_.size(); ++i) {
                for (typename std::list<KeyT>::const_iterator it =
                         buckets_[i].begin();
                     it != buckets_[i].end();
                     ++it) {
                    tmp.Insert(*it);
                }
            }
            *this = tmp;
        }
    }
};

#endif // UNORDERED_SET_UNORDERED_SET_H
